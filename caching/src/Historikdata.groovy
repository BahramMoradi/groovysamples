import groovy.transform.ToString

/**
 * Created by bam on 01-02-2018.
 */
@ToString
class Historikdata {
    Date dagendato
    String payload
}
