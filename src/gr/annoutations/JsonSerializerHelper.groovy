package gr.annoutations

import java.lang.reflect.Field


class JsonSerializerHelper {
    public static  <T> Map toMap(T t, Class annotationClass, String annuotationClassFieldName = "jsonFieldName"){
        Map map = [:]
        List<Field> fields = t.getClass().getDeclaredFields().toList().findAll {!it.synthetic && it.isAnnotationPresent(annotationClass)}
        fields.each {field->
            String  jsonFieldNameFromAnnotation = null
            if(annuotationClassFieldName){
                jsonFieldNameFromAnnotation = field.getAnnotation(annotationClass)."$annuotationClassFieldName"()
            }
            String jsonFieldName = jsonFieldNameFromAnnotation?:field.name
            if(field.isAnnotationPresent(EnumType.class)){
                println "enum type detected"
                map[jsonFieldName] =  t."$field.name".name()
            }else{
                map[jsonFieldName] = t."$field.name"
            }


        }

        println map

        return map
    }

}
