package objectsandproperties

import models.Person


/**
 * Created by bam on 13-09-2018.
 */
List<Person> ls = [
        new Person(navn: "A",birthday:new Date()),
        new Person(navn: "B",birthday:new Date().plus(-10)),
        new Person(navn: "B",birthday:new Date().plus(-50))
]

List<Person> empty = []
println ls.max {it.birthday}.toString()
println empty.max {it.birthday}