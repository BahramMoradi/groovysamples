package dk.bam.gro.dsl

import groovy.time.TimeCategory

import java.lang.reflect.Field
import java.text.SimpleDateFormat

/**
 * Created by bam on 10-11-2018.
 */
class Main {


    def static set(String field) {
        [of: { Object obj ->
            [to: { String value ->

                Field f = obj.class.getDeclaredField(field)
                f.setAccessible(true)
                f.set(obj, value)

            }]
        }]
    }

    def static format(Date date) {
        [to: { String format -> new SimpleDateFormat(format).format(date) }]

    }


    def static add(int dage) {
        [dag: { Date date ->
                    use(TimeCategory) {
                        date.plus(dage)
                    }
                }
        ]
    }

    public static void main(String... args) {
        User user = new User()

        set "name" of user to "Bahram"

        Date nu = new Date()

        String formattedDate = format nu to "yyyy-MM-dd"
        println(formattedDate)


        Date nyDate = add 10 dag nu
        println(nu)
        println(nyDate)

// equivalent to: please(show).the(square_root).of(100)
        //Set set = new Set()
        //User uder = new User()
        //set.field("name").of(user).to("Bahram")


    }
}
