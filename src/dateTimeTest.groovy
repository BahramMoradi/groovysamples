/**
 * Created by bam on 19-09-2017.
 */
public List<Date> hentDatoList(Date fra, Date dagsdato) {
    fra = fra.clearTime()
    dagsdato = dagsdato.clearTime()
    List datoer = []
    if (fra <= dagsdato) {
        for (dato in (fra..dagsdato)) {
            datoer << dato
        }
    }
    return datoer
}

def lsr = new Date(year: 2017, month: Calendar.SEPTEMBER, date: 15,
        hours: 23, minutes: 30, seconds: 0)


def now =  new Date(year: 2017, month: Calendar.SEPTEMBER, date: 19,
        hours: 9, minutes: 15, seconds: 35)

lsr = new Date()-5
now = new Date()
hentDatoList(lsr,now).each {it->
    println it.format("dd-MM-yyyy")
}