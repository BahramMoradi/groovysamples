package dk.tree

import groovy.transform.ToString

@ToString
class Node {
    public Node(String name,Integer value,List<Node> childs){
        this.name= name
        this.value = value
        this.childs = childs
    }

    public String name
    public Integer value
    public List<Node> childs = []
}
