import groovy.transform.ToString

/**
 * Created by bam on 01-02-2018.
 */
@ToString
class DriftStatus {
    Date start
    Date slut
    String status
}
