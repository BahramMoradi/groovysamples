package enumtest


enum SamtykkeKoder implements Serializable {

    SA01("samtykkeerklæring"),
    SA02("serviceattest"),
    SA03("straffeattest"),
    SA04("erhvervsstyrelsen"),
    SA05("","navn"),
    SA06("undertegnede erklærer"),
    SA07("undertegnede giver"),
    SA08("ønsker ikke"),
    SA09("Afgiv"),
    SA10("declaration of consent"),
    SA11("service certificate"),
    SA12("criminal record certificate"),
    SA13("the Danish Business Authority"),
    SA14("I consent"),
    SA15("not been convicted"),
    SA16("do not wish"),
    SA17("issue")

    public String statiskTekst
    private String attribut

    public SamtykkeKoder(String statiskTekst, String attribut = null) {
        this.statiskTekst = statiskTekst
        this.attribut = attribut
    }
}
