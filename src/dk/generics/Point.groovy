package dk.generics
import static java.lang.Math.*
class Point<T> {
    T x
    T y

    Double distanceTo(Point<T> other){
        double  xdis = (this.x-other.x)* (this.x-other.x)
        double ydis = (this.y-other.y)* (this.y-other.y)
        Double distance = sqrt(xdis+ydis)
        return distance
    }
}
