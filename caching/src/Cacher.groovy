/**
 * Created by bam on 01-02-2018.
 */
class Cacher<T> {
    int timeToLive = 20 // default 300 second
    private long cachedTimeMiliSecond = 0
    private T cachedData = null

    public T hentCachedData(){
        return  isLessThanTimeToLiveAndGreaterThanZero()? cachedData : null
    }


    public void updateCache(T data){
        println "cache updated.."
        cachedTimeMiliSecond = System.currentTimeMillis()
        cachedData = data
    }
    private long getTimeDuration(){
        return (System.currentTimeMillis()-cachedTimeMiliSecond)/1000
    }
    private boolean isLessThanTimeToLiveAndGreaterThanZero(){
        long timeDuration = getTimeDuration()
        return  timeDuration > 0 && timeDuration <= timeToLive
    }
}
