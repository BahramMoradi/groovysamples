package dk.tree



public Node findMax(Node node, Node max){

    println node?.name+" | "+max?.name

    if(!node){
        max
    }

    if(!max){
        max = node
    }

    if(node.value >  max.value){
        max = node
    }

    node.childs.each {it->
        max = findMax(it,max)
    }


    return max

}


Node a , b, c, d ,e ,f
f = new Node("F",8,[])
e = new Node("E",2,[])
d = new Node("D",7,[])
c = new Node("C",3,[f])
b = new Node("B",6,[d,e])
a = new Node("A",5,[b,c]);


Node mx = findMax(a,null)

println "---------------------------"
println (mx.name+" "+mx.value)

