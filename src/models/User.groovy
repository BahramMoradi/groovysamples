package models

import groovy.transform.ToString

/**
 * Created by bam on 13-10-2018.
 */
@ToString
class User {
    String name
    String email
    int age
}
