/**
 * Created by bam on 01-02-2018.
 */

Cacher historikdataCacher = new Cacher<Historikdata>()
Cacher driftStatusCacher = new Cacher<List<DriftStatus>>()


Historikdata hd = new Historikdata(dagendato: new Date(), payload: "Det er min payload")

List<DriftStatus> ls = [
        new DriftStatus(start: new Date()-10,slut: new Date()-4,status:"NED"),
        new DriftStatus(start: new Date()-8,slut: new Date()-2,status:"OPPE"),
        new DriftStatus(start: new Date()-5,slut: new Date()-1,status:"OPPE"),

]


historikdataCacher.updateCache(hd)
driftStatusCacher.updateCache(ls)

println "sleep for 10 s"
sleep(10000)

println historikdataCacher.hentCachedData()
println driftStatusCacher.hentCachedData()

println "sleep for 12 s"
sleep(12000)
println historikdataCacher.hentCachedData()
println driftStatusCacher.hentCachedData()



