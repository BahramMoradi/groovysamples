package spreadtest

/**
 * Created by bam on 14-02-2018.
 */
import java.security.MessageDigest

private static String genererNoegle(String rutNummer){
    if (rutNummer == null || rutNummer == "") return null
    // statisk salt key – bør kun kendes af ATP, ERST og Netcompany
    String salt = "ATP2016OGERST2016";
    //Generer nøgle via md5
    MessageDigest md5 = MessageDigest.getInstance("MD5")
    md5.update((salt + rutNummer).getBytes())
    return new BigInteger(1,md5.digest()).toString(16).padLeft(32,"0").substring(0,8)
}


println genererNoegle("R0023976")