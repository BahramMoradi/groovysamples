package gr.annoutations


class Tjenste {
    @PublishToErstDist(jsonFieldName = "id")
    long id
    @PublishToErstDist
    String title
    @PublishToErstDist(jsonFieldName = "gyldigfra")
    Date date
    @JsonEnum
    Sesson sesson = Sesson.SUMMER

}
