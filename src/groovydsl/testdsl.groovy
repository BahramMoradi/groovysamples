package groovydsl

import models.User
import org.junit.runners.Parameterized


public <T> T newInstance(T t){
    return t.newInstance()
}

User newUser = newInstance(User.class)
newUser.with {
    name = "Diman"
    email = "dim@gmail.com"
    age = 34
}


User user  = new User()
user.with {
    setName "Bahram"
    setEmail "bam@gmail.com"
    setAge 45
}

println user

user.with {
    name = "Daniel"
    email ="dani2016@gmail.com"
    age = 2
}


println user



