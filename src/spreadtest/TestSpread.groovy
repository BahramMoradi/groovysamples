package spreadtest

/**
 * Created by bam on 02-11-2017.
 */
String f1="ABCDEFGH"
String f2="IJKLMNOP"
List<Funktionsomraade> fom1= []
List<Funktionsomraade> fom2= []
f1.each {it-> fom1 << new Funktionsomraade(id:it,navn: it)}
f2.each {it-> fom2 << new Funktionsomraade(id:it,navn: it)}
List<Tilladelse> tilladelser = [
        new Tilladelse(funktionsomraadeList:fom1),
        new Tilladelse(funktionsomraadeList:fom2 )
]

List<String> all = tilladelser*.getFunktionsomraaderNavne()?.flatten()
println all

